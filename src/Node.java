import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Node {
    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node(String n, Node d, Node r) {
        // Regex for a proper name.
        Pattern p = Pattern.compile("^[^,()\\s]+$");
        Matcher m = p.matcher(n);

        // Throw an exception if the name is malformed.
        if (!m.find()) {
            throw new RuntimeException("Node name contains illegal symbols. Provided: " + n + ".");
        }

        name = n;
        firstChild = d;
        nextSibling = r;
    }

    public static Node parsePostfix(String s) {
        // String tokenizer.
        StringTokenizer st = new StringTokenizer(s, "(,)", true);

        // Stack of array lists, each representing a level of a tree.
        Stack<ArrayList<Node>> nodeStack = new Stack<>();

        // Root level.
        nodeStack.add(new ArrayList<>());

        // First node of the level that was just popped.
        Node lastLevelStart = null;

        // Last token.
        String lastToken = null;

        // Iterating through parsed tokens.
        while (st.hasMoreElements()) {
            String token = (String) st.nextElement();

            switch (token) {
                case ",":
                    // Commas cannot be at the beginning of the string, follow an opened bracket or follow another
                    // comma.
                    if (lastToken == null || lastToken.equals(",") || lastToken.equals("(") || lastToken.equals(")")) {
                        throw new RuntimeException("Malformed RPR. Provided: `" + s + "`.");
                    }

                    //// ((A),(C)D)E

                    break;
                case "(":
                    if (lastToken != null) {
                        if (lastToken.equals(")")) {
                            throw new RuntimeException("Malformed RPR. Provided: `" + s + "`.");
                        }

                        if (!(lastToken.equals(",") || lastToken.equals("("))) {
                            throw new RuntimeException("Malformed RPR. Provided: `" + s + "`.");
                        }
                    }

                    // ((A)B(C)D)E


                    // Start of the new level in the stack.
                    nodeStack.push(new ArrayList<>());

                    break;
                case ")":
                    // End of the level in the stack.
                    ArrayList<Node> a = nodeStack.pop();

                    // Size of the array list cannot be 0 and last level cannot be popped if another level was
                    // popped without having any name. Prevents stuff like `()a` or `(b())a`.
                    if (a.size() < 1 || lastLevelStart != null) {
                        throw new RuntimeException("Malformed RPR. Provided: `" + s + "`.");
                    }

                    lastLevelStart = a.get(0);

                    break;
                default:
                    // Current level of the tree.
                    ArrayList<Node> curLevel = nodeStack.peek();

                    // Last sibling on the current level. If the level is currently empty, then the variable
                    // is set to null.
                    Node lastNode = curLevel.size() > 0 ? curLevel.get(curLevel.size() - 1) : null;

                    // Creating new node. In case of a malformed name, throw an exception.
                    Node newNode;

                    try {
                        newNode = new Node(token, null, null);
                    } catch (RuntimeException e) {
                        throw new RuntimeException("Malformed RPR. Provided: `" + s + "`.");
                    }

                    // If last node exists, then setting the next sibling value of the last node to be pointed to
                    // the new node.
                    if (lastNode != null) {
                        lastNode.nextSibling = newNode;
                    }

                    // If it's the first node after a level was popped, then setting the first child value of the
                    // new node to the starting node of the last level.
                    if (lastLevelStart != null) {
                        newNode.firstChild = lastLevelStart;
                        lastLevelStart = null;
                    }

                    // Adding the new node to the current level.
                    curLevel.add(newNode);

                    break;
            }

            // Updating last token.
            lastToken = token;
        }

        // Getting the root of the stack. In a non-malformed RPR, this should be the last element in the stack.
        ArrayList<Node> root = nodeStack.pop();

        // If the size of the root level is either empty, contains more than two elements, or the last level was
        // popped without having a name, then throw an exception.
        if (root.size() != 1 || lastLevelStart != null) {
            throw new RuntimeException("Malformed RPR. Provided: `" + s + "`.");
        }

        // Returning the root node.
        return root.get(0);
    }

    public String leftParentheticRepresentation() {
        // Running the recursive method on itself.
        return leftParentheticRepresentation(this);
    }

    private String leftParentheticRepresentation(Node root) {
        StringBuilder lpr = new StringBuilder();
        lpr.append(root.name);

        Node current = root.firstChild;
        if (current == null) {
            return lpr.toString();
        }

        lpr.append('(');
        while (current != null) {
            lpr.append(leftParentheticRepresentation(current));

            if (current.nextSibling != null) {
                lpr.append(',');
            }

            current = current.nextSibling;
        }
        lpr.append(')');

        return lpr.toString();
    }

    public static void main(String[] param) {
        Node a = new Node("a", null, null);
        Node h2 = new Node("h2", a, null);
        Node h1 = new Node("h1", null, h2);
        Node li_2 = new Node("li_2", null, null);
        Node li_1 = new Node("li_1", null, li_2);
        Node ul = new Node("ul", li_1, h1);
        Node body = new Node("body", ul, null);
        Node title = new Node("title", null, null);
        Node meta = new Node("meta", null, title);
        Node head = new Node("head", meta, body);
        Node html = new Node("html", head, null);

        System.out.println("Manual tree LPR: " + html.leftParentheticRepresentation());
        String s = "((meta,title)head,((li_1,li_2)ul,h1,(a)h2)body)html";
        System.out.println("Parsed tree RPR: " + s);

        Node t = Node.parsePostfix(s);
        System.out.println("Generated LPR: " + t.leftParentheticRepresentation());

        Node t2 = Node.parsePostfix("(1)a");
        System.out.println(parsePostfix("((A)B(C)D)E").leftParentheticRepresentation());
        // ((A),(C)D)E
        // ((A)(C)D)E
        // ((A)B(C)D)E
    }
}
